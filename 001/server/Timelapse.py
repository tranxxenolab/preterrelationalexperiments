import glob, os

from flask import Blueprint, send_file
import json

imagePath = "/media/physarum/PreterrelationalExperiments/001/time-lapse/"

timelapse_api = Blueprint("timelapse_api", __name__)

@timelapse_api.route("/latest")
def getLatest():
    # Get sorted list of files from imagePath
    # TODO
    # This is probably super inefficient for large number of files
    sorted_files = sorted(glob.iglob(imagePath + "Physari-Algo-006*.png"))
    latest = sorted_files[-1]

    # Ensure that our last image is actually an image, and wasn't just a placeholder as the camera was taking the image.
    size = os.path.getsize(latest)
    if (size <= 0):
        latest = sorted_files[-2]

    j = json.dumps({'latest': latest})
    return send_file(latest, mimetype="image/png")

@timelapse_api.route("/latest/video")
def getLatestVideo():
    latest = max(glob.iglob(imagePath + "video.webm"), key=os.path.getctime)
    j = json.dumps({'latest': latest})
    return send_file(latest, mimetype="video/webm")
