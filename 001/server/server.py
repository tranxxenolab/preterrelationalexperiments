
from flask import Flask
from Timelapse import timelapse_api

import glob, os
imagePath = "/media/starfields/PreterrelationalExperiments/001/time-lapse/"


app = Flask(__name__)
app.register_blueprint(timelapse_api, url_prefix="/timelapse")

@app.route("/")
def hello():
    return "Hello World!"

if __name__ == "__main__":
    app.run(host="0.0.0.0", port = "5002")
