#!/usr/bin/env python

import time
from time import sleep
import signal
from datetime import datetime, timedelta
import pickle
import argparse
import os, sys

import yaml

import RPi.GPIO as GPIO

# Need to have started daemon already: `sudo pigpio`
import pigpio

# Uncomment for production
#GPIO.setwarnings(False)

class LEDControl(object):
    """Control the LEDs by fading them in and out over certain durations."""
    # TODO
    # rewrite docstring for __init__
    def __init__(self, config = "./conf/leds.yaml",
            path = "./",
            maxBrightness = 0.2,
            maxDCValue = 255.0,
            fadeDur = 30.0 * 60.0,
            waitDur = 10.0 * 60.0,
            interval = 3.0,
            expPrefix = "Physari-Algo"):
        """Initialize the LED control.
        
        Parameters
        ----------
        config: str
            Type of image to take; allowed values are 'IR' or 'visible'. Defaults to 'IR'.
        interval : int
            Interval between images in minutes
        IRControlPin : int
            Which pin the control line for the IR LEDs is connected to. Given according to GPIO.BCM.
        visibleControlPin : int
            Which pin the control line for the visible LEDs is connected to. Given according to GPIO.BCM
        path : str
            Where to store the time-lapse images
        expPrefix : str
            Prefix to give this experiment
        """

        # Save parameters
        self.config =config 
        self.path = path
        self.maxBrightness = maxBrightness
        self.maxDCValue = maxDCValue
        self.fadeDur = fadeDur
        self.waitDur = waitDur
        self.interval = interval
        self.expPrefix = expPrefix

        # Amount to change duty cycle each interval
        self.deltaDC = (self.maxBrightness * self.maxDCValue)/(self.fadeDur / self.interval)

        # What is our current duty cycle
        self.currentDC = 0.0
        
        # What is the max DC we want
        self.maxDC = self.maxBrightness * self.maxDCValue
        
        # What is the min DC we want
        self.minDC = 0.0
       

        print("Fading in/out at %f per %f seconds" % (self.deltaDC, self.interval))

        # Whether we're fading down or not
        self.fadeDown = False
        
        # Whether we're waiting or not
        self.waiting = False
        

        # LED pins
        with open(self.config, "r") as f:
            self.leds = yaml.load(f, Loader = yaml.FullLoader)

        self.currentLEDPin = 0

        # Data dictionary for pickling
        self.data = {}
        self.data["PWM_info"] = {
            "maxBrightness": self.maxBrightness,
            "maxDCValue": self.maxDCValue,
            "fadeDur": self.fadeDur,
            "waitDur": self.waitDur,
            "interval": self.interval
                }
        self.data['PWM'] = []

        # Pickle filename
        self.pickleFilename = self.expPrefix + "_LEDs_" + datetime.now().strftime("%Y-%m-%d-%H-%M-%S") + ".pickle"
        self.picklePath = os.path.join(self.path, self.pickleFilename)
        self.lastPickleWriteTime = time.time()
        self.pickleWriteDur = 60.0

        # Setup PWM
        self.pwm = pigpio.pi()
        self.pwm.set_PWM_dutycycle(self._getLEDPin(), 0)

        # Setup signal handler
        signal.signal(signal.SIGTERM, self.sigterm)
        # TODO
        # Figure out how to deal with SIGKILL
        #signal.signal(signal.SIGKILL, self.sigterm)

    def sigterm(self, _signo, _stack_frame):
        """Deal with being terminated."""
        self.handle_close()
        sys.exit(0)

    def handle_close(self):
        """Handle exits."""
        print("Cleaning things up")
        self.pwm.set_PWM_dutycycle(self._getLEDPin(), 0)
        self.pwm.stop()

    def _getLED(self):
        """Internal function to get the current LED. We move through the LEDs in a round-robin fashion."""

        return (self.currentLEDPin % len(self.leds))

    def _getLEDPin(self):
        """Internal function to get the current LED pin, properly typecast."""
        
        return int(self.leds[self._getLED()]["pin"])

    def _calculateDelay(self):
        """Internal function to calculate the delay or sleep time. Uses `interval` as defined in the constructor.
    
        Returns
        -------
        int
            Amount of time to delay by
    
        Taken from picamera documentation: <https://picamera.readthedocs.io/en/release-1.13/recipes1.html>
        """
    
        now = datetime.now()
        next_interval = (now + timedelta(seconds = self.interval)).replace(microsecond = 0)
        delay = (next_interval - now).total_seconds()
        #delay = delay / 1000000.0
        print("Delaying for %f seconds" % delay)
        return delay

    def doLEDControl(self):
        try:
            while True:
                # TODO
                # Probably need some kind of transfer function to map the range
                # to different duty cycle values
    
                if not self.waiting:
                    # Write pickle to file occasionally
                    if ((time.time()) > (self.lastPickleWriteTime + self.pickleWriteDur)):
                        pickle.dump(self.data, open(self.picklePath, "wb"))
                        self.lastPickleWriteTime = time.time()
    
                    if not self.fadeDown:
                        if (self.currentDC <= self.maxDC):
                            print("Current duty cycle: %f" % self.currentDC)
                            self.pwm.set_PWM_dutycycle(self._getLEDPin(), self.currentDC)
                            now = datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
                            self.data['PWM'].append((now, "{0:.2f}".format(self.currentDC), self._getLEDPin(), self.leds[self._getLED()]["color"]))
                            sleep(self._calculateDelay())
                            self.currentDC += self.deltaDC
                        else:
                            self.currentDC = self.maxDC
                            self.fadeDown = True
                            self.waiting = True
                    else:
                        if (self.currentDC >= self.minDC):
                            print("Current duty cycle: %f" % self.currentDC)
                            self.pwm.set_PWM_dutycycle(self._getLEDPin(), self.currentDC)
                            now = datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
                            self.data['PWM'].append((now, "{0:.2f}".format(self.currentDC), self._getLEDPin(), self.leds[self._getLED()]["color"]))
                            sleep(self._calculateDelay())
                            self.currentDC -= self.deltaDC
                        else:
                            self.currentDC = self.minDC
                            self.fadeDown = False
                            self.waiting = True
                            self.currentLEDPin += 1
                else:
                    sleep(self._calculateDelay())
                    self.waiting = False

            pass
        except KeyboardInterrupt:
            self.handle_close()
            sys.exit(0)
        except Exception as arg:
            print("Error: % s" % arg)
            self.handle_close()
            sys.exit(0)
        finally:
            print("done, finally")
            self.handle_close()
            sys.exit(0)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description = "Options for controlling LEDs")
    parser.add_argument("-c", "--config", dest="config", type=str, default = "./conf/leds.yaml", help="YAML config file for LEDs. See source code for explanation of the format. Defaults to './conf/leds.yaml'")
    # basePath = "/home/pi/PreterrelationalExperiments/001/data"
    parser.add_argument("-p", "--path", dest="path", type=str, default = "./", help="Path to store pickled data. Defaults to './'.")
    parser.add_argument("--maxbrightness", dest="maxbrightness", type=float, default = 0.2, help="Maximum brightness of each LED, as a fraction of the total duty cycle. Defaults to 0.2.")
    parser.add_argument("--maxdcvalue", dest="maxdcvalue", type=float, default = 255.0, help="Maximum duty cycle value, which is dependent on the library used. For pigpio, the max is 255.0; for RPi.GPIO, it's 100.0. Defaults to 255.0.")
    parser.add_argument("--fadedur", dest="fadedur", type=float, default = 30.0 * 60.0, help="Duration over which we fade the LED in and out, in seconds. Defaults to 30 min (30m * 60s/min).")
    parser.add_argument("--waitdur", dest="waitdur", type=float, default = 10.0 * 60.0, help="How long to wait at the end of each fade in or fade out. Defaults to 10min (10min * 60s/min).")
    parser.add_argument("--interval", dest="interval", type=float, default = 3.0, help="How often to update the duty cycle, in seconds. Defaults to 3.0.")
    parser.add_argument("-e", "--expprefix", dest="expprefix", type=str, default = "Physari-Algo", help="Experimental prefix to use. Defaults to 'Physari-Algo'.")

    args = parser.parse_args()

    ledControl = LEDControl(config = args.config,
            path = args.path,
            maxBrightness = args.maxbrightness,
            maxDCValue = args.maxdcvalue,
            fadeDur = args.fadedur,
            waitDur = args.waitdur,
            interval = args.interval,
            expPrefix = args.expprefix)
    ledControl.doLEDControl()

    sys.exit(0)
