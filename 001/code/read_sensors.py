#!/usr/bin/env python

import time
from time import sleep
import signal
from datetime import datetime, timedelta
import pickle
import argparse
import os, sys

import yaml

import board
import busio
from adafruit_ads1x15.analog_in import AnalogIn

import Adafruit_DHT


class Sensors(object):
    """Read from our sensors"""

    # TODO
    # rewrite docstring for __init__
    def __init__(self, config = "./conf/sensors.yaml",
            path = "./",
            interval = 2.0,
            expPrefix = "Physari-Algo"):
        """Initialize the sensor reader.
        
        Parameters
        ----------
        config: str
            Type of image to take; allowed values are 'IR' or 'visible'. Defaults to 'IR'.
        interval : int
            Interval between images in minutes
        IRControlPin : int
            Which pin the control line for the IR LEDs is connected to. Given according to GPIO.BCM.
        visibleControlPin : int
            Which pin the control line for the visible LEDs is connected to. Given according to GPIO.BCM
        path : str
            Where to store the time-lapse images
        expPrefix : str
            Prefix to give this experiment
        """

        # Save parameters
        self.config =config 
        self.path = path
        self.interval = interval
        self.expPrefix = expPrefix

        # LED pins
        with open(self.config, "r") as f:
            self.sensors= yaml.load(f, Loader = yaml.FullLoader)

        print(self.sensors)

        # Which sensors are we reading from?
        self.adc = False
        self.th = False

        self.i2c = busio.I2C(board.SCL, board.SDA)


        # Data dictionary for pickling
        self.data = {}
        self.data["sensor_info"] = {
            "interval": self.interval
                }
        self.data['sensors'] = {}
        
        self._parseSensorConfig()

        # Pickle filename
        self.pickleFilename = self.expPrefix + "_sensors_" + datetime.now().strftime("%Y-%m-%d-%H-%M-%S") + ".pickle"
        self.picklePath = os.path.join(self.path, self.pickleFilename)
        self.lastPickleWriteTime = time.time()
        self.pickleWriteDur = 10.0

        # Setup signal handler
        signal.signal(signal.SIGTERM, self.sigterm)
        # TODO
        # Figure out how to deal with SIGKILL
        #signal.signal(signal.SIGKILL, self.sigterm)

    def sigterm(self, _signo, _stack_frame):
        """Deal with being terminated."""
        self.handle_close()
        sys.exit(0)

    def handle_close(self):
        """Handle exits."""
        print("Cleaning things up")


    def _calculateDelay(self):
        """Internal function to calculate the delay or sleep time. Uses `interval` as defined in the constructor.
    
        Returns
        -------
        int
            Amount of time to delay by
    
        Taken from picamera documentation: <https://picamera.readthedocs.io/en/release-1.13/recipes1.html>
        """
    
        now = datetime.now()
        next_interval = (now + timedelta(seconds = self.interval)).replace(microsecond = 0)
        delay = (next_interval - now).total_seconds()
        #delay = delay / 1000000.0
        print("Delaying for %f seconds" % delay)
        return delay

    def _parseSensorConfig(self):
        """Parse the sensor config info and set things up accordingly."""

        try:
            if(self.sensors["ADC"]["enabled"]):
                self.adc = True

                self.data['sensors']["ADC"] = {}
                self.data['sensors']["ADC"]["info"] = self.sensors["ADC"]
                self.data['sensors']["ADC"]["data"] = []

                self.adc_type = self.sensors["ADC"]["adc_type"]
                self.adc_pins = self.sensors["ADC"]["pins"]
                self.adc_gain = self.sensors["ADC"]["gain"]

                if (self.adc_type == "ADS1015"):
                    import adafruit_ads1x15.ads1015 as ADS
                    self.ads = ADS.ADS1015(self.i2c)
                elif (self.adc_type == "ADS1115"):
                    import adafruit_ads1x15.ads1115 as ADS
                    self.ads = ADS.ADS1115(self.i2c)

                self.adc_mapping = {
                    0: ADS.P0, 
                    1: ADS.P1, 
                    2: ADS.P2, 
                    3: ADS.P3 
                }

                if (self.adc_gain == "2/3"):
                    self.ads.gain = 2/3
                else:
                    self.ads.gain = self.adc_gain
            
            if(self.sensors["th"]["enabled"]):
                self.th = True
                self.thPin = self.sensors["th"]["pin"]

                self.data['sensors']["th"] = {}
                self.data['sensors']["th"]["info"] = self.sensors["th"]
                self.data['sensors']["th"]["data"] = []
                self.lastTHData = (None, None)

        except KeyError:
            pass

    def readSensors(self):
        """Read from the chosen sensors."""
        try:
            while True:
                # Write pickle to file occasionally
                if ((time.time()) > (self.lastPickleWriteTime + self.pickleWriteDur)):
                    pickle.dump(self.data, open(self.picklePath, "wb"))
                    self.lastPickleWriteTime = time.time()

                if (self.adc):
                    now = datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
                    vdata = {}
                    # TODO deal with this properly through the conf file
                    chan = AnalogIn(self.ads, self.adc_mapping[0], self.adc_mapping[1])
                    vdata[0] = (chan.value, chan.voltage)
                    chan = AnalogIn(self.ads, self.adc_mapping[2], self.adc_mapping[3])
                    vdata[1] = (chan.value, chan.voltage)

                    """
                    for pin in self.adc_pins:
                        chan = AnalogIn(self.ads, self.adc_mapping[pin])
                        vdata[pin] = (chan.value, chan.voltage)
                    """
                    
                    self.data['sensors']["ADC"]["data"].append([now, vdata])
                    print(vdata)
    
                if (self.th):
                    h, t = Adafruit_DHT.read(11, self.thPin)
                    if (h is not None):
                        self.lastTHData = (h, t)
                        now = datetime.now().strftime("%Y-%m-%d-%H-%M-%S")

                        print("Temp: {0:0.1f} Humidity: {1:0.1f}".format(t, h))
                    
                        self.data['sensors']["th"]["data"].append([now, (h, t)])
                    else:
                        print("Didn't get any data.")
                    pass

                sleep(self._calculateDelay())

        except KeyboardInterrupt:
            self.handle_close()
            sys.exit(0)
        except Exception as arg:
            print("Error: % s" % arg)
            self.handle_close()
            sys.exit(0)
        finally:
            print("done, finally")
            self.handle_close()
            sys.exit(0)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description = "Options for reading crom the sensors")
    parser.add_argument("-c", "--config", dest="config", type=str, default = "./conf/sensors.yaml", help="YAML config file for the sensors. See source code for explanation of the format. Defaults to './conf/sensors.yaml'")
    parser.add_argument("-p", "--path", dest="path", type=str, default = "./", help="Path to store pickled data. Defaults to './'.")
    parser.add_argument("--interval", dest="interval", type=float, default = 2.0, help="How often to read from the sensors, in seconds. Defaults to 2.0.")
    parser.add_argument("-e", "--expprefix", dest="expprefix", type=str, default = "Physari-Algo", help="Experimental prefix to use. Defaults to 'Physari-Algo'.")

    args = parser.parse_args()

    sensors = Sensors(config = args.config,
            path = args.path,
            interval = args.interval,
            expPrefix = args.expprefix)
    sensors.readSensors()

    sys.exit(0)
