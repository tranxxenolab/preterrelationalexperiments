#!/usr/bin/env python
import signal
from datetime import datetime, timedelta
from time import sleep
from fractions import Fraction
import argparse
import os, sys

from picamera import PiCamera
import RPi.GPIO as GPIO


# See here for some interesting things about the camera:
# https://raspberrypi.stackexchange.com/questions/58871/pi-camera-v2-fast-full-sensor-capture-mode-with-downsampling

# Force sensor mode 3 (the long exposure mode), set
# the framerate to 1/6fps, the shutter speed to 6s,
# and ISO to 800 (for maximum gain)

class TimeLapse(object):
    """Take a continuous time-lapse. Handles signals gracefully so as to properly release resources."""

    def __init__(self, imageType = "IR", 
            interval = 2, 
            IRControlPin = 23,
            visibleControlPin = 24,
            path = "./",
            expPrefix = "Physari-Algo"):
        """Initialize the time-lapse.
        
        Parameters
        ----------
        imageType : str
            Type of image to take; allowed values are 'IR' or 'visible'. Defaults to 'IR'.
        interval : int
            Interval between images in minutes
        IRControlPin : int
            Which pin the control line for the IR LEDs is connected to. Given according to GPIO.BCM.
        visibleControlPin : int
            Which pin the control line for the visible LEDs is connected to. Given according to GPIO.BCM
        path : str
            Where to store the time-lapse images
        expPrefix : str
            Prefix to give this experiment
        """

        # Save parameters
        self.imageType = imageType
        self.interval = interval
        self.IRControlPin = IRControlPin
        self.visibleControlPin = visibleControlPin
        self.path = path
        self.expPrefix = expPrefix

        # Camera is not setup yet
        self._camera_setup = False

        # Current LED state
        self._ledsOn = False

        # Setup signal handler
        signal.signal(signal.SIGTERM, self.sigterm)
        # TODO
        # Figure out how to deal with SIGKILL
        #signal.signal(signal.SIGKILL, self.sigterm)

    def sigterm(self, _signo, _stack_frame):
        """Deal with being terminated."""
        self.handle_close()
        sys.exit(0)

    def _calculateDelay(self):
        """Internal function to calculate the delay or sleep time. Uses `interval` as defined in the constructor.

        Returns
        -------
        int
            Amount of time to delay by

        Taken from picamera documentation: <https://picamera.readthedocs.io/en/release-1.13/recipes1.html>
        """

        next_interval = (datetime.now() + timedelta(minutes = self.interval)).replace(second = 5, microsecond = 0)
        delay = (next_interval - datetime.now()).total_seconds()
        print("Delaying for %f seconds" % delay)
        return delay

    def _toggleLEDs(self):
        """Toggle the LEDs on and off for the given image mode."""

        if (self._ledsOn):
            if (self.imageType == "IR"):
                GPIO.output(self.IRControlPin, GPIO.LOW)
            elif (self.imageType == "visible"):
                GPIO.output(self.visibleControlPin, GPIO.LOW)
            self._ledsOn = False
        else:
            if (self.imageType == "IR"):
                GPIO.output(self.IRControlPin, GPIO.HIGH)
            elif (self.imageType == "visible"):
                GPIO.output(self.visibleControlPin, GPIO.HIGH)
            self._ledsOn = True

    def handle_close(self):
        """Handle exits."""
        print("cleaning things up, hopefully")

        # Cleanup camera
        self.cleanupCamera()

        print("Cleaning up GPIO")
        self.cleanupGPIO()

    def setupGPIO(self):
        """Setup our GPIO pins."""

        # Set our mode
        GPIO.setmode(GPIO.BCM)

        # Set relevant outputs
        if (self.imageType == "IR"):
            GPIO.setup(self.IRControlPin, GPIO.OUT, initial=GPIO.LOW)
            #GPIO.output(self.IRControlPin, GPIO.HIGH)
        elif(self.imageType == "visible"):
            GPIO.setup(self.visibleControlPin, GPIO.OUT, initial=GPIO.LOW)
            #GPIO.output(self.visibleControlPin, GPIO.HIGH)

    def setupCamera(self, **kwargs):
        """Setup our camera. **Call only once**. Will sleep a long time to let camera settle.
        
        Parameters
        ----------
        \*\*kwargs: int
            shutter_speed : shutter speed, in microseconds. If given, overrides the faults of 2000000 for IR and 350000 for visible
        """

        if self._camera_setup is False:
            print("Setting up camera")
            self.camera = PiCamera(framerate = Fraction(1, 6),
                    resolution = (1640, 1232),
                    sensor_mode = 4)

            print("Setting up camera parameters")
            try:
                self.camera.shutter_speed = kwargs['shutter_speed']
            except KeyError:
                if (self.imageType == "IR"):
                    self.camera.shutter_speed = 2500000
                elif (self.imageType == "visible"):
                    self.camera.shutter_speed = 150000
                else:
                    self.camera.shutter_speed = 3000000
            #self.camera.shutter_speed = shutter_speed
            print("Shutter speed: %d" % self.camera.shutter_speed)
            self.camera.iso = 100
            self.camera.rotation = 180
            #self.camera.start_preview()

            if (self.imageType == "IR"):
                sleep_time = 30
            elif (self.imageType == "visible"):
                sleep_time = 10
            else:
                sleep_time = 30

            # Give the camera a good long time to set gains and
            # measure AWB (you may wish to use fixed AWB instead)
            print("Sleeping for %is to wait for camera to settle" % sleep_time)
            self.camera.awb_mode = "auto"
            self._toggleLEDs()
            sleep(sleep_time)

            self.camera.awb_mode = "off"
            self._toggleLEDs()
            
            self.camera.exposure_mode = 'off'

            # These awb settings are subject to change, but seem to work 
            # at the moment
            if (self.imageType == "visible"):
                self.camera.awb_gains = (Fraction(393, 256), Fraction(61, 32))
            elif (self.imageType == "IR"):
                self.camera.awb_gains = (Fraction(395, 256), Fraction(245, 128))
            self._camera_setup = True
        else:
            pass


    def cleanupGPIO(self):
        """Cleanup GPIO pins, including setting any control pins to `GPIO.LOW`."""
        if (self.imageType == "IR"):
            GPIO.output(self.IRControlPin, GPIO.LOW)
        elif(self.imageType == "visible"):
            GPIO.output(self.visibleControlPin, GPIO.LOW)
       
        GPIO.cleanup()

    def cleanupCamera(self):
        """Cleanup the camera."""
        
        if self._camera_setup:
            #self.camera.stop_preview()
            print("Closing camera")
            self.camera.close()
            print("Camera closed")
        else:
            print("Nothing to cleanup for the camera")

    def doTimeLapse(self):
        try:
            self.setupGPIO()
            self.setupCamera()

            if self._camera_setup:
                while True:
                    now = datetime.now()
                    t = now.strftime("%Y-%m-%d-%H-%M")
                    filename = os.path.join(self.path, self.expPrefix + "_" + self.imageType + "_" + t + ".png")
                    print("Capturing to %s" % filename)
                    self._toggleLEDs()
                    self.camera.capture(filename)
                    self._toggleLEDs()
                    sleep(self._calculateDelay())
        except KeyboardInterrupt:
            self.handle_close()
            sys.exit(0)
        except Exception as arg:
            print("Error: % s" % arg)
            self.handle_close()
            sys.exit(0)
        finally:
            print("done, finally")
            sys.exit(0)


## CRUFT
#def take_image(imageType):
#    print("Setting up IR LED")
#    GPIO.setmode(GPIO.BCM)
#    GPIO.setup(23, GPIO.OUT, initial=GPIO.LOW)
#    GPIO.output(23, GPIO.HIGH)
#    
#    try:
#        print("Setting up camera")
#        camera = PiCamera(framerate = Fraction(1, 6),
#                resolution = (3280, 2464),
#                sensor_mode = 3)
#        camera.shutter_speed = 2000000
#        camera.iso = 100
#        camera.start_preview()
#        # Give the camera a good long time to set gains and
#        # measure AWB (you may wish to use fixed AWB instead)
#        print("Sleeping for 30s")
#        sleep(30)
#        camera.exposure_mode = 'off'
#        # Finally, capture an image with a 6s exposure. Due
#        # to mode switching on the still port, this will take
#        # longer than 6 seconds
#        print("Taking photo")
#        camera.capture('dark.png')
#        camera.stop_preview()
#        # This needs to be a long-ass time, especially when using full resolution PNGs,
#        # otherwise the camera.close() below will hang nastily
#        # Seems to require 35s so as to allow enough time for things to be released.
#        # Or whatever is going on behind the scenes.
#        sleep(50)
#    
#       
#    finally:
#        print("Closing camera")
#        camera.close()
#    
#        print("Turning off IR LED")
#        GPIO.output(23, GPIO.LOW)
#    
#        print("Cleaning up GPIO")
#        GPIO.cleanup()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description = "Photo taking options for time-lapse. Will begin an endless time-lapse. Type ctrl-c or kill process to end it and close the camera cleanly.")
    parser.add_argument("-t", "--type", dest="imageType", type=str, choices = ["visible", "IR"], default = "IR", help="Type of image to take. Options are 'visible' or 'IR'. Defaults to 'IR'")
    parser.add_argument("-p", "--path", dest="path", type=str, default = "./", help="Path to store images. Defaults to './'.")
    parser.add_argument("-i", "--interval", dest="interval", type=int, default = 2, help="Interval between images in minutes. Defaults to 2m.")
    parser.add_argument("-r", "--irpin", dest="irpin", type=int, default = 23, help="Which pin the IR control line is connected to, according to BCM. Defaults to 23.")
    parser.add_argument("-s", "--visiblepin", dest="visiblepin", type=int, default = 24, help="Which pin the visible control line is connected to, according to BCM. Defaults to 24.")
    parser.add_argument("-e", "--expprefix", dest="expprefix", type=str, default = "Physari-Algo", help="Experimental prefix to use. Defaults to 'Physari-Algo'.")

    args = parser.parse_args()
    print("Image type: %s" % args.imageType)

    timeLapse = TimeLapse(imageType = args.imageType, 
            path = args.path,
            interval = args.interval,
            visibleControlPin = args.visiblepin,
            IRControlPin = args.irpin,
            expPrefix = args.expprefix)
    timeLapse.doTimeLapse()
    sys.exit(0)
