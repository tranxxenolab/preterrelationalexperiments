#!python
import glob, os

from skimage.measure import compare_ssim
#import imutils
import cv2

PATH = "/mnt/d/PreterrelationalExperiments/001/time-lapse"
CVPATH = "/mnt/d/PreterrelationalExperiments/001/cv"
STEM = "Physari-Algo_visible_*.png"

print("globbing files")
files = glob.glob(os.path.join(PATH, STEM))

files.sort()

subset = files[-480:]

for x in range(0, len(subset) - 1):
    try:
        imageA = cv2.imread(subset[x])
        imageB = cv2.imread(subset[x+1])
    
        grayA = cv2.cvtColor(imageA, cv2.COLOR_BGR2GRAY)
        grayB = cv2.cvtColor(imageB, cv2.COLOR_BGR2GRAY)
    
        (score, diff) = compare_ssim(grayA, grayB, full = True)
        diff = (diff * 255).astype("uint8")
        print("SSIM: {}".format(score))
    
        thresh = cv2.threshold(diff, 0, 255, cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]
    
        isWritten = cv2.imwrite(os.path.join(CVPATH, "diff_%03d.png" % x), thresh)
    except cv2.error as e:
        print("Error: {}".format(e))
        print("Files: %s and %s" % (subset[x], subset[x+1]))
        # TODO need to fix whatever is going on here
        continue
