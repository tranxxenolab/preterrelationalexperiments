#!/usr/bin/env python

import time
from time import sleep
from datetime import datetime, timedelta
import pickle
import os
import RPi.GPIO as GPIO

# Need to have started daemon already: `sudo pigpio`
import pigpio

# For DHT11 humidity, temp sensor
# Thank goodness they wrote the code for dealing with the timing of this sensor!
import Adafruit_DHT

# Uncomment for production
#GPIO.setwarnings(False)

def _calculateDelay(interval):
    """Internal function to calculate the delay or sleep time. Uses `interval` as defined in the constructor.

    Returns
    -------
    int
        Amount of time to delay by

    Taken from picamera documentation: <https://picamera.readthedocs.io/en/release-1.13/recipes1.html>
    """

    now = datetime.now()
    next_interval = (now + timedelta(seconds = interval)).replace(microsecond = 0)
    delay = (next_interval - now).total_seconds()
    #delay = delay / 1000000.0
    print("Delaying for %f seconds" % delay)
    return delay

# Data dictionary
data = {}
data['PWM'] = []
data['temp_humidity'] = []

# Pickle filename
basePath = "/home/pi/PreterrelationalExperiments/001/data"
filename = datetime.now().strftime("%Y-%m-%d-%H-%M-%S") + ".pickle"
picklePath = os.path.join(basePath, filename)

# Max brightness, in percentage of 100% duty cycle
maxBrightness = 0.2

# What is the maximum value for the duty cycle?
maxDCValue = 255.0

# Duration to fade in and out, in seconds
fadeDuration = 30.0 * 60.0
#fadeDuration = 1.0 * 60.0

# Interval to update duty cycle, in seconds
interval = 3.0

# Amount to change duty cycle each interval
deltaDC = (maxBrightness * maxDCValue)/(fadeDuration / interval)

print("Fading in/out at %f per %f seconds" % (deltaDC, interval))

# What is our current duty cycle
currentDC = 0.0

# What is the max DC we want
maxDC = maxBrightness * maxDCValue

# What is the min DC we want
minDC = 0.0

# How long do we wait, in seconds
waitDur = 10.0 * 60.0

# LED pins
ledPins = [20, 16, 18]
currentLEDPin = 0

# Whether we're fading down or not
fadeDown = False

# Whether we're waiting or not
waiting = False

lastPickleWriteTime = time.time()
pickleWriteDur = 60.0



if __name__ == "__main__":
    print("beginning PWM")
   
    p1 = pigpio.pi()
    
    p1.set_PWM_dutycycle(ledPins[currentLEDPin], 0)
    
    try:
        while True:
#            if ((int(time.time()) % 10) == 0):
#                # args: sensor type (11 for 3 pin), pin number
#                # TODO
#                # looks like this will need to live in a separate thread,
#                # as it looks like it blocks during reads, which is just
#                # not gonna work
#                # Or, maybe we use the version that doesn't do retries
#                humidity, temp = Adafruit_DHT.read_retry(11, 21)
#                now = datetime.now()
#                now_str = now.strftime("%Y-%m-%d %H:%M:%S")
#    
#                print("%s: Temp: %0.1f C Humidity %0.1f %%" % (now_str, temp, humidity))
    
            # TODO
            # Make the fade out slower at the lower end of the ranges
            # Probably need some kind of transfer function to map the range
            # to different duty cycle values
            # Also, ensure that there is some time between the fade out and the
            # next fade in


            if not waiting:
                # Write pickle to file occasionally
                if ((time.time()) > (lastPickleWriteTime + pickleWriteDur)):
                    pickle.dump(data, open(picklePath, "wb"))
                    lastPickleWriteTime = time.time()

                if not fadeDown:
                    if (currentDC <= maxDC):
                        print("Current duty cycle: %f" % currentDC)
                        p1.set_PWM_dutycycle(ledPins[currentLEDPin], currentDC)
                        now = datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
                        data['PWM'].append((now, currentDC, ledPins[currentLEDPin]))
                        sleep(_calculateDelay(interval))
                        currentDC += deltaDC
                    else:
                        currentDC = maxDC
                        fadeDown = True
                        waiting = True
                else:
                    if (currentDC >= minDC):
                        print("Current duty cycle: %f" % currentDC)
                        p1.set_PWM_dutycycle(ledPins[currentLEDPin], currentDC)
                        now = datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
                        data['PWM'].append((now, currentDC, ledPins[currentLEDPin]))
                        sleep(_calculateDelay(interval))
                        currentDC -= deltaDC
                    else:
                        currentDC = minDC
                        fadeDown = False
                        waiting = True
                        currentLEDPin += 1
            else:
                sleep(_calculateDelay(waitDur))
                waiting = False

    except KeyboardInterrupt:
        pass
    finally:    
        print("cleaning up")
        p1.set_PWM_dutycycle(ledPin, 0)
        p1.stop()
